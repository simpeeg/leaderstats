﻿using System.Collections.Generic;
using System.Linq;
using LeaderStats.Importation;

namespace LeaderStats.Models.Indexes
{
    public class SalaryIndex
    {
        public static Dictionary<int, List<Leader>> _index = new Dictionary<int, List<Leader>>();

        public static Dictionary<int, List<Leader>> Index
        {
            get
            {
                if (_index.Count == 0)
                {
                    IndexNow();
                }

                return _index;
            }
        }

        public static void IndexNow()
        {
            var sortedBySalary = Importer.Leaders.AsParallel().OrderBy(x => x.Salary).ToList();

            var lst = new List<Leader>();
            var lastLeaderSalary = sortedBySalary.First().Salary;

            foreach (var leader in sortedBySalary)
            {
                if (leader.Salary != lastLeaderSalary)
                {
                    _index.Add(lastLeaderSalary, lst);
                    lst = new List<Leader>();
                    lastLeaderSalary = leader.Salary;
                }

                lst.Add(leader);
            }
        }
    }
}
