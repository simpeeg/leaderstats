﻿using System.Collections.Generic;
using System.Linq;
using LeaderStats.Importation;

namespace LeaderStats.Models.Indexes
{
    public class NameIndex
    {
        public static Dictionary<string, List<Leader>> _index = new Dictionary<string, List<Leader>>();

        public static Dictionary<string, List<Leader>> Index
        {
            get
            {
                if (_index.Count == 0)
                {
                    IndexNow();
                }

                return _index;
            }
        }

        public static void IndexNow()
        {
            var sortedByName = Importer.Leaders.AsParallel().OrderBy(x => x.Name).ToList();

            var lst = new List<Leader>();
            var lastLeaderName = sortedByName.First().Name;

            foreach (var leader in sortedByName)
            {
                if (leader.Name != lastLeaderName)
                {
                    _index.Add(lastLeaderName, lst);
                    lst = new List<Leader>();
                    lastLeaderName = leader.Name;
                }

                lst.Add(leader);
            }
        }
    }
}