﻿using System.Collections.Generic;
using System.Linq;
using LeaderStats.Importation;

namespace LeaderStats.Models.Indexes
{
    public class ExperienceIndex
    {
        private static Dictionary<int, List<Leader>> _index = new Dictionary<int, List<Leader>>();

        public static Dictionary<int, List<Leader>> Index
        {
            get
            {
                if (_index.Count == 0)
                {
                    IndexNow();
                }

                return _index;
            }
        }

        public static void IndexNow()
        {
            var sortedByExperience = Importer.Leaders.AsParallel().OrderBy(x => x.Experience).ToList();

            var lst = new List<Leader>();
            var lastLeaderExperience = sortedByExperience.First().Experience;

            foreach (var leader in sortedByExperience)
            {
                if (leader.Experience != lastLeaderExperience)
                {
                    _index.Add(lastLeaderExperience, lst);
                    lst = new List<Leader>();
                    lastLeaderExperience = leader.Experience;
                }

                lst.Add(leader);
            }
        }
    }
}
