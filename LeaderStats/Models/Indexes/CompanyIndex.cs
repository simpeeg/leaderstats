﻿using System.Collections.Generic;
using System.Linq;
using LeaderStats.Importation;

namespace LeaderStats.Models.Indexes
{
    public class CompanyIndex
    {
        private static Dictionary<string, List<Leader>> _index = new Dictionary<string, List<Leader>>();

        public static Dictionary<string, List<Leader>> Index
        {
            get
            {
                if (_index.Count == 0)
                {
                    IndexNow();
                }

                return _index;
            }
        }

        public static void IndexNow()
        {
            var sortedCompany = Importer.Leaders.AsParallel().OrderBy(x => x.Company).ToList();

            var lst = new List<Leader>();
            var lastCompanyname = sortedCompany.First().Company.ToLower();

            foreach (var leader in sortedCompany)
            {
                if (leader.Company.ToLower() != lastCompanyname)
                {
                    _index.Add(lastCompanyname, lst);
                    lst = new List<Leader>();
                    lastCompanyname = leader.Company.ToLower();
                }

                lst.Add(leader);
            }
        }
    }
}