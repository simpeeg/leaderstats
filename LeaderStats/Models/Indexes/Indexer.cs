﻿
namespace LeaderStats.Models.Indexes
{
    public class Indexer
    {
        public static bool HasBeenIndex { get; private set; }

        public static void IndexEverything()
        {
            AgeIndex.IndexNow();
            CompanyIndex.IndexNow();
            ExperienceIndex.IndexNow();
            NameIndex.IndexNow();
            SalaryIndex.IndexNow();

            HasBeenIndex = true;
        }
    }
}