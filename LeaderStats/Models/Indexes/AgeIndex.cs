﻿using System.Collections.Generic;
using System.Linq;
using LeaderStats.Importation;

namespace LeaderStats.Models.Indexes
{
    public class AgeIndex
    {
        private static Dictionary<int, List<Leader>> _index = new Dictionary<int, List<Leader>>();

        public static Dictionary<int, List<Leader>> Index
        {
            get
            {
                if (_index.Count == 0)
                {
                    IndexNow();
                }

                return _index;
            }
        }

        public static void IndexNow()
        {
            var sortedByAge = Importer.Leaders.AsParallel().OrderBy(x => x.Age).ToList();

            var lst = new List<Leader>();
            var lastLeaderAge = sortedByAge.First().Age;

            foreach (var leader in sortedByAge)
            {
                if (leader.Age != lastLeaderAge)
                {
                    _index.Add(lastLeaderAge, lst);
                    lst = new List<Leader>();
                    lastLeaderAge = leader.Age;
                }

                lst.Add(leader);
            }
        }
    }
}
