﻿
namespace LeaderStats.Models
{
    public class Leader
    {
        public string Company { get; set; }
        public string Ticker { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Experience { get; set; }
        public int Salary { get; set; }
    }
}