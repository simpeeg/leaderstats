﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaderStats.Models.ViewModels
{
    public class IndexViewModel
    {
        public bool IsIndexed { get; set; }
    }
}