﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaderStats.Models.ViewModels
{
    public class AgeFraction
    {
        public string Label { get; set; }
        public double Fraction { get; set; }
    }
}