﻿using System.Collections.Generic;

namespace LeaderStats.Models.ViewModels
{
    public class CompanyViewModel
    {
        public string Name { get; set; }
        public string Ticker { get; set; }
        public List<Leader> Employees { get; set; }
    }
}