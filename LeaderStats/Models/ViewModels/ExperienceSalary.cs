﻿namespace LeaderStats.Models.ViewModels
{
    public class ExperienceSalary
    {
        public int Experience { get; set; }
        public int Salary { get; set; }
    }
}