﻿using LeaderStats.Models;

namespace LeaderStats.Importation
{
    public class LeaderFactory
    {
        public static Leader FromRecord(string record)
        {
            var fields = record.Split('\t');

            return new Leader
                       {
                           Company = fields[0],
                           Ticker = fields[1],
                           Name = fields[2],
                           Age = int.Parse(fields[3]),
                           Experience = int.Parse(fields[4]),
                           Salary = int.Parse(fields[5])
                       };
        }
    }
}