﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using LeaderStats.Models;

namespace LeaderStats.Importation
{
    public class Importer
    {
        private static IList<Leader> _leaders = new List<Leader>();

        public static IList<Leader> Leaders
        {
            get
            {
                if (_leaders.Count == 0)
                {
                    var filePath = HostingEnvironment.MapPath("~/Models/dataset.csv");
                    Import(filePath);
                }
                return _leaders;
            }
        }

        private static void Import(string filePath)
        {
            var records = File.ReadAllLines(filePath);

            _leaders = records.Select(LeaderFactory.FromRecord).ToList();

        }
    }
}

