﻿
Raphael.fn.drawGrid = function (x, y, w, h, wv, hv, color) {
    color = color || "#000";
    var path = ["M", Math.round(x) + .5, Math.round(y) + .5, "L", Math.round(x + w) + .5, Math.round(y) + .5, Math.round(x + w) + .5, Math.round(y + h) + .5, Math.round(x) + .5, Math.round(y + h) + .5, Math.round(x) + .5, Math.round(y) + .5],
        rowHeight = h / hv,
        columnWidth = w / wv;
    for (var i = 1; i < hv; i++) {
        path = path.concat(["M", Math.round(x) + .5, Math.round(y + i * rowHeight) + .5, "H", Math.round(x + w) + .5]);
    }
    for (i = 1; i < wv; i++) {
        path = path.concat(["M", Math.round(x + i * columnWidth) + .5, Math.round(y) + .5, "V", Math.round(y + h) + .5]);
    }
    return this.path(path.join(",")).attr({stroke: color});
};

jQuery.fn.createHistogram = function () {
	var labels = [],
		data = [];
	$("#data tfoot th").each(function () {
		labels.push($(this).html());
	});
	$("#data tbody td").each(function () {
		data.push($(this).html());
	});

	var width = 800,
		height = 250,
		leftgutter = 30,
		bottomgutter = 20,
		topgutter = 20,
		colorhue = .6 || Math.random(),
		color = "hsl(" + [colorhue, .5, .5] + ")",
		r = Raphael("holder2", width, height),
		txt = { font: '12px Helvetica, Arial', fill: "#fff" },
		txt1 = { font: '10px Helvetica, Arial', fill: "#fff" },
		txt2 = { font: '12px Helvetica, Arial', fill: "#000" },
		X = (width - leftgutter) / labels.length,
		max = Math.max.apply(Math, data),
		Y = (height - bottomgutter - topgutter) / max,
		divNum = labels.length,
		gridHeight = height - topgutter - bottomgutter;
	r.drawGrid(0, topgutter + .5, width - leftgutter - X, height - topgutter - bottomgutter, 10, 10, "#000");


	var label = r.set(),
        lx = 0, ly = 0,
        is_label_visible = false,
        leave_timer,
        blanket = r.set();

	var rects = new Array(divNum);

	for (var i = 0; i < divNum; i++) {
		var y = Math.round(height - bottomgutter - Y * data[i]),
            x = Math.round((width - leftgutter - X) / divNum * (i)),
            t = r.text(x + ((width - leftgutter - X) / divNum / 2), height - 6, labels[i]).attr(txt).toBack();
		rects[i] = r.rect(
					(width - leftgutter - X) / divNum * (i), (height - bottomgutter) - (gridHeight * data[i] / max),
					 (width - leftgutter - X) / divNum, gridHeight * data[i] / max);
		rects[i].attr("fill", "#00f");
	}

}