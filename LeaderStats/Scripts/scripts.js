﻿
jQuery.fn.updatePie = function (data) {
	$("#PieChart").empty();
	$("#PieChart").append("<tbody></tbody>");
	$.each(data, function (key, val) {
		$("<tr></tr>").appendTo($("#PieChart > tbody")).append('<th scope="row">' + val.Label + '</th>').append("<td>" + val.Fraction * 100 + "%</td>");
	});

	$().createPie();
};

jQuery.fn.createPie = function () {
	$("#pieHolder").empty();
	var values = [],
	labels = [];
	$("tr").each(function () {
		values.push(parseInt($("td", this).text(), 10));
		labels.push($("th", this).text());
	});
	$("table").hide();
	Raphael("pieHolder", 700, 700).pieChart(350, 350, 200, values, labels, "#fff");
};

$(function () {
	//Create pie on page load
	if ($("#pieHolder").length > 0) {
		$().createPie();
	}


	//OnClick Activate Pie
	$("#activatePie").click(function () {
		$.ajax({
			url: "/Leader/GetAges",
			dataType: "json",
			success: function (data) {
				$().updatePie(data);
			}
		});
	});
	// Autocomplete
	$("#keywords").autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "/Company/CompanyAutoComplete",
				dataType: "json",
				data: {
					count: 10,
					searchTerm: request.term
				},
				success: function (data) {
					console.log('SSS');
					response(data);
				}
			});
		},
		select: function (event, ui) {
			console.log(ui);
			$('input[name="term"]').val(ui.item.label);
			$("form").submit();
		},
		minLength: 2
	});
});

