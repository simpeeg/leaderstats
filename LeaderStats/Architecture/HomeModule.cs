﻿using LeaderStats.Models.Indexes;
using LeaderStats.Models.ViewModels;

namespace LeaderStats.Architecture
{
    public class HomeModule
    {
        public IndexViewModel GetIndexState()
        {
            return new IndexViewModel
                       {
                           IsIndexed = Indexer.HasBeenIndex
                       };
        }
    }
}