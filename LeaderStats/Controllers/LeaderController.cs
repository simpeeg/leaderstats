﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LeaderStats.Models;
using LeaderStats.Models.Indexes;
using LeaderStats.Models.ViewModels;

namespace LeaderStats.Controllers
{
    public class LeaderController : Controller
    {
        //public JsonResult Ages()
        //{
        //    var ageFractions = BuildAgeFractions();

        //    return Json(ageFractions, JsonRequestBehavior.AllowGet);
        //}

        public PartialViewResult Ages()
        {
            var ageFractions = BuildAgeFractions();

            return PartialView(ageFractions);
        }

        public PartialViewResult ExperienceSalary()
        {
            var lol = BuildExperienceSalary().ToList();
            return PartialView(lol);
        }


        private IEnumerable<ExperienceSalary> BuildExperienceSalary()
        {
            var index = ExperienceIndex.Index;

            return index.Select(experienceRange => new ExperienceSalary
                                                       {
                                                           Salary = (int) experienceRange.Value.Average(x => x.Salary),
                                                           Experience = experienceRange.Key
                                                       });
        }

        private static IEnumerable<AgeFraction> BuildAgeFractions()
        {
            var leaders = AgeIndex.Index;


            var from0to20 = leaders.Where(leader => leader.Key >= 0 && leader.Key <= 20);
            var from21to30 = leaders.Where(leader => leader.Key >= 21 && leader.Key <= 30);
            var from31to40 = leaders.Where(leader => leader.Key >= 31 && leader.Key <= 40);
            var from41to50 = leaders.Where(leader => leader.Key >= 41 && leader.Key <= 50);
            var from51to60 = leaders.Where(leader => leader.Key >= 51 && leader.Key <= 60);
            var from61to70 = leaders.Where(leader => leader.Key >= 61 && leader.Key <= 70);
            var from71to80 = leaders.Where(leader => leader.Key >= 71 && leader.Key <= 80);

            return new List<AgeFraction>
                       {
                           new AgeFraction
                               {
                                   Label = "0 à 20",
                                   Fraction = CalculateFraction(from0to20)
                               },
                           new AgeFraction
                               {
                                   Label = "21 à 30",
                                   Fraction = CalculateFraction(from21to30)
                               },
                           new AgeFraction
                               {
                                   Label = "31 à 40",
                                   Fraction = CalculateFraction(from31to40)
                               },
                           new AgeFraction
                               {
                                   Label = "41 à 50",
                                   Fraction = CalculateFraction(from41to50)
                               },
                           new AgeFraction
                               {
                                   Label = "51 à 60",
                                   Fraction = CalculateFraction(from51to60)
                               },
                           new AgeFraction
                               {
                                   Label = "61 à 70",
                                   Fraction = CalculateFraction(from61to70)
                               },
                           new AgeFraction
                               {
                                   Label = "71 à 80",
                                   Fraction = CalculateFraction(from71to80)
                               },
                       };
        }

        private static int CountForRange(IEnumerable<KeyValuePair<int, List<Leader>>> range)
        {
            return range.Sum(age => age.Value.Count);
        }

        private static double CalculateFraction(IEnumerable<KeyValuePair<int, List<Leader>>> range)
        {
            var leadersCount = (double)Importation.Importer.Leaders.Count;
            var crude = CountForRange(range) / leadersCount;

            return Math.Round(crude, 2);
        }
    }
}