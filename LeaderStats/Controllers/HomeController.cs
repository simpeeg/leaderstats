﻿using System.Web.Mvc;
using LeaderStats.Architecture;
using LeaderStats.Models.Indexes;

namespace LeaderStats.Controllers
{
    public class HomeController : Controller
    {
        private HomeModule _module;
        //
        // GET: /Home/

        public HomeController()
        {
            _module = new HomeModule();
        }

        public ActionResult Index()
        {
            return View(_module.GetIndexState());
        }

        public RedirectResult IndexNow()
        {
            Indexer.IndexEverything();
            ViewBag.IsIndexed = true;
            return Redirect("Index");
        }
    }
}
