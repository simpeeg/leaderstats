﻿using System.Linq;
using System.Web.Mvc;
using LeaderStats.Models.ViewModels;

namespace LeaderStats.Controllers
{
    public class CompanyController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Search(string term)
        {
            if(!Models.Indexes.CompanyIndex.Index.ContainsKey(term.ToLower()))
            {
                return View(new CompanyViewModel());
            }

            var company = Models.Indexes.CompanyIndex.Index[term.ToLower()];
            var viewModel = new CompanyViewModel
                                {
                                    Name = company.First().Company,
                                    Ticker = company.First().Ticker,
                                    Employees = company
                                };
            return View(viewModel);
        }

        public JsonResult CompanyAutoComplete(string searchTerm, int count)
        {
            var keys = Models.Indexes.CompanyIndex.Index.Keys;
            var results = keys.Where(key => key.ToLower().StartsWith(searchTerm.ToLower())).ToList();

            if (count == 0)
                count = results.Count;

            return Json(results.Take(count), JsonRequestBehavior.AllowGet);
        }

    }
}
